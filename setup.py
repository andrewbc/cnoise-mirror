#!/usr/bin/env python
from distutils.core import setup, Extension

setup(name="cNoise",
      version="0.0.2",
      description="A perlin and simplex noise module, implemented in C.",
      author="AndrewBC",
      author_email="andrew.b.coleman@gmail.com",
      url="http://andrewbc.net/",
      ext_modules=[
        Extension("cNoise", sources=["cNoisemodule.c"]),
      ])
